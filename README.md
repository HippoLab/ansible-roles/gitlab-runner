## Usage

Example playbook

```yaml
- hosts:
    - gitlab_runners
  become: true
  roles:
    - name: docker-engine
      tags:
        - docker
        - docker-engine
    - name: docker-cleanup
      tags:
        - docker
        - docker-cleanup
    - name: gitlab-runner
      tags:
        - gitlab
        - gitlab-runner
  tasks:
    - name: Add gitlab-runner user to docker group
      ansible.builtin.user:
        name: gitlab-runner
        groups: docker
    - name: Install Amazon credentials helper
      apt:
        name: amazon-ecr-credential-helper
        state: latest
      tags:
        - docker
        - aws
```

Example runners
```yaml
gitlab_runners:
  - name: "london_shell"
    gitlab_url: "https://gitlab.com/"
    executor:
      name: "shell"
      opts: ""
    registration_token: "{{ vault_gitlab_registration_token }}"
    tags:
      - london
      - shell
      - uk
  - name: "london_docker"
    gitlab_url: "https://gitlab.com/"
    executor:
      name: "docker"
      opts:
        - "--docker-image=alpine:latest"
    registration_token: "{{ vault_gitlab_registration_token }}"
    tags:
      - london
      - docker
      - uk
```
